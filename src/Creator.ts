import {Service} from './Service.js';
import {Product} from './Product.js';
export abstract class Creator{
  protected myProduct:Product;
  protected myService:Service;
  constructor(){
    console.log("Clase Creator");
  }

  public createProductsAndServices():void{
    this.myProduct = this.product();
    this.myService = this.service();
  }

  protected abstract product():Product;
  protected abstract service():Service;
}
