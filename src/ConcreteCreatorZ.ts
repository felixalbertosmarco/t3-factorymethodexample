import {ConcreteProductZ} from './ConcreteProductZ.js';
import {ConcreteServiceZ} from './ConcreteServiceZ.js';
import {Creator} from './Creator.js';
import {Product} from './Product.js';
import {Service} from './Service.js';
export class ConcreteCreatorZ extends Creator{
  constructor(){
    super();
  }
  protected product():Product{
    return new ConcreteProductZ();
  }
  protected service():Service{
    return new ConcreteServiceZ();
  }
}
