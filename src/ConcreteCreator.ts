import {ConcreteProduct} from './ConcreteProduct.js';
import {ConcreteService} from './ConcreteService.js';
import {Creator} from './Creator.js';
import {Product} from './Product.js';
import {Service} from './Service.js';
export class ConcreteCreator extends Creator{
  constructor(){
    super();
  }
  protected product():Product{
    return new ConcreteProduct();
  }
  protected service():Service{
    return new ConcreteService();
  }
}
